#!/usr/bin/env sh
#
#
#

# ------------------------------------------------------
# user configuration
config_path="$HOME/conf/mwaccess.keycloak.json";
log_path="$HOME/logs/de.dkfz.adc.mwaccess.log"
log_level="ALL";
# ------------------------------------------------------

PRG="$0";
#echo $PRG; # ./mwaccess.sh

SCRIPT_DIR=`dirname "$0"`;
SCRIPT_DIR_FULL="$( readlink -f ${SCRIPT_DIR}  )";
#echo $SCRIPT_DIR; # .
#echo $SCRIPT_DIR_FULL; # /home/amann/eclipse-workspace/mwaccess

CURRENTUSER=$(who | awk 'NR==1{print $1}')
#echo $CURRENTUSER

LOGFILE_NAME="mwaccess.log"
if [ ! -e $SCRIPT_DIR/$LOGFILE_NAME  ];then
        touch $SCRIPT_DIR/$LOGFILE_NAME
        #chown $CURRENTUSER:$CURRENTUSER $SCRIPT_DIR/$LOGFILE_NAME
fi

function log () {
        local msg=$1
        echo $msg
        local dt=`date +%F_%T`
        echo "$dt $msg" >> $SCRIPT_DIR/$LOGFILE_NAME
}

function wait () {
        arg1=$1
        arg2=$2
        secs=$arg2
        #secs=$((5 * 60))
        while [ $secs -gt 0 ]; do
                echo -ne "wait $arg2 secs (${arg1}) ... $secs\033[0K\r"
                sleep 1
                : $((secs--))
        done
}

APP_NAME="Main"
APP_BASE_NAME=`basename "$0"`
APP_HOME="`pwd -P`"

# Add default JVM options here. You can also use JAVA_OPTS and MWACCESS_OPTS to pass JVM options to this script.
DEFAULT_JVM_OPTS='"-Xmx64m" "-Xms64m"'

CLASSPATH=".:$CLASSPATH:$SCRIPT_DIR_FULL/target/mwaccess-0.0.1-SNAPSHOT-jar-with-dependencies.jar";
echo CLASSPATH=$CLASSPATH

# /usr/lib/jvm/java-11-openjdk
# /usr/lib/jvm/java-1.8.0-openjdk

# Determine the Java command to use to start the JVM.
JAVA_HOME=/usr/lib/jvm/java-11-openjdk

if [ -z ${JAVA_HOME+x}  ]; then
	echo "Please set the JAVA_HOME variable in your environment to match the location of your Java installation."
	exit -1
else
	echo JAVA_HOME: $JAVA_HOME
fi

JAVACMD="$JAVA_HOME/bin/java";

APP_ARGS="$@";
echo arguments: $APP_ARGS; 

eval set -- $DEFAULT_JVM_OPTS $JAVA_OPTS $MWACCESS_OPTS -classpath "\"$CLASSPATH\"" de.dkfz.adc.mwaccess.Main "$APP_ARGS"

exec "$JAVACMD" "$@" --config-path="$config_path" --log-path="$log_path" --log-level=ALL



