/**
 * mwaccess accesses adc-middleware protected study data from command line
 * Main
 * ..
 * https://www.keycloak.org/docs/16.1/securing_apps/index.html#_installed_adapter
 * The application needs to be configured as a public OpenID Connect client with 
 * Standard Flow Enabled and http://localhost as an allowed Valid Redirect URI.
 * ..
 * 
 * GNU LESSER GENERAL PUBLIC LICENSE
 * 
 */
package de.dkfz.adc.mwaccess;
import java.io.*;
import java.net.URISyntaxException;
import java.util.*;
import java.util.logging.*;

import org.keycloak.OAuthErrorException;
import org.keycloak.adapters.ServerRequest.HttpFailure;
import org.keycloak.adapters.installed.KeycloakInstalled;
import org.keycloak.common.VerificationException;
import org.keycloak.representations.AccessToken;

import static java.lang.System.out;


/**
 * @author amann
 *
 */
public class Main {

	// arguments and descriptions
	private static final Map<String, String> ARGUMENTS = new HashMap<String, String>();
	
	static {
		ARGUMENTS.put("--config-path", "full path to the keycloak configuration file");
		ARGUMENTS.put("--log-path", "full path to logging file");
		ARGUMENTS.put("--log-level", "controls logging output (defaults to Level.ALL)");
		ARGUMENTS.put("--list-studies", "lists studies");
		
		ARGUMENTS.put("--help", "shop command line arguments");
	}	
	
	// store command line arguments
	private static String[] argsGiven;
	
	// the main instance
	private static Main INSTANCE; 

	public Main() {
		if ( INSTANCE == null )
			Main.INSTANCE = this;
	}
	
	/* https://docs.oracle.com/javase/8/docs/technotes/guides/logging/overview.html */
	private static final String LOGGER_NAME="de.dkfz.adc.mwaccess";
	
	private  Logger logger = Logger.getLogger(LOGGER_NAME);	
	
	// access environment variables
	private Map<String, String> env = System.getenv();
	
	// full path to config file, default $HOME/mwacccess.keycloak.json
	private String ConfigPath =  env.get("HOME") +  "/mwacccess.keycloak.json";

	// full path to log file, default $HOME/mwacccess.keycloak.json
	private String LogPath = "/tmp/"+Main.LOGGER_NAME+".log";	

	// keycloak context
	private KeycloakInstalled keycloak;
	
	// users access token
	AccessToken token = null;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Main main = new Main();
		Main.argsGiven = args;
		
		//out.println( Arrays.toString(args) );
		
		// initiate logger
		if (! main.initLogger()) { 
			out.println("cannot prepare log file."); 
			System.exit(-1);
		}
		// initiate keycloak context
		main.createKeycloakContext();

		if (!main.loginForToken());
			System.exit(-1);
		
		main.token = main.keycloak.getToken();
		out.println(main.token.toString());
		
		if (!main.logoutFromKeycloak() );
			System.exit(-1);		
		
		out.println("main end.");
		main.logger.info("main end.");
	}

	
	
	public boolean loginForToken() {
		try {
			INSTANCE.keycloak.loginDesktop();
		} catch (InterruptedException| IOException | VerificationException | OAuthErrorException | URISyntaxException | HttpFailure e) {
			e.printStackTrace();
			logger.warning("unable to login " + e.getCause() + " " + e.getMessage());
			return false;
		}
		return true;
	}
	
	public boolean logoutFromKeycloak() {
		try {
			INSTANCE.keycloak.logout();
		} catch (IOException | InterruptedException | URISyntaxException e) {
			e.printStackTrace();
			logger.warning("unable to logout " + e.getCause() + " " + e.getMessage());
			return false;
		}
		return true;
	}
	
	/*
	 * get a config value from the arguments or null if not given
	 * */
	private static Optional<String> getConfigValueFromArgs( String ConfigKeyToFind ) {
		for( String argElem : Main.argsGiven ) {
			String[] argElemArr = argElem.split("=");
			
			if( argElemArr[0].equals(ConfigKeyToFind) ) {
				
				//out.println( "found: " +  argElemArr[0]);
				return Optional.of( argElemArr[1] ) ;
			}
		}
		return Optional.ofNullable(null);
	}	
	
	/*
	 * configures the logger for file output, leve
	 * */
	private boolean initLogger() {
		try {
			
			LogManager manager = LogManager.getLogManager();
			manager.readConfiguration(new FileInputStream ( "src/main/resources/mwaccess-logging.properties" ));
			
			// (1) check conf file
			Main.getConfigValueFromArgs( "--log-path" ).ifPresent( s -> this.LogPath=s );
			
			File f = new File(this.LogPath);
			if (!f.exists()) {
				f.createNewFile();
			}else {
				// TODO check size and rotate 
			}
			
			// append to given file
			FileHandler fh = new FileHandler( this.LogPath, true );
			fh.setFormatter( new SimpleFormatter() );
			logger.addHandler(fh);
			// (2) level
			Main.getConfigValueFromArgs( "--log-level" ).ifPresent( s -> 
			{
				switch (s) {
					case "INFO": this.logger.setLevel(Level.INFO); break;
					case "SEVERE": this.logger.setLevel(Level.SEVERE); break;
					case "CONFIG": this.logger.setLevel(Level.CONFIG); break;
					case "FINE": this.logger.setLevel(Level.FINE); break;
					case "FINER": this.logger.setLevel(Level.FINER); break;
					case "FINEST": this.logger.setLevel(Level.FINEST); break;
					case "ALL": this.logger.setLevel(Level.ALL); break;
					case "OFF": this.logger.setLevel(Level.OFF); break;
					default: this.logger.setLevel(Level.INFO); break;
				}
			}
					);
		}catch (IOException ioe) {
			ioe.printStackTrace(); return false;
		}
		return true;
	}
	

	
	/* create keycloak context from configuration file */
	private void createKeycloakContext() {
		
		Main.getConfigValueFromArgs( "--config-path" ).ifPresent( s -> this.ConfigPath=s );
		
		//out.println( this.ConfigPath );
		
		File f = new File(this.ConfigPath);
		if ( ! f.exists() ) {
			this.logger.severe("configuration file not found: " + f.toString());
			System.exit(-1);
		}
				
		try (InputStream in = new FileInputStream( f ) ) {
			this.keycloak = new KeycloakInstalled( in );
		} catch ( IOException ioe ) {
			ioe.printStackTrace();
			logger.severe("Could not read keycloak configuration");
			System.exit(-1);
		}
	}
	
	
}
